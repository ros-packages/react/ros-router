# ros-router
#### It is to wrap _react-router-dom_

sample of the code 
```js
import rosRouter from "ros-router";

rosRouter.create([
    { name:"home",path:"/home", component:{<div>Home</div>}},
    { name:"app",path:"/app/*", component:{<div>Home</div>} , 
        children:[
            { name:"dashboard",path:"dashboard", component:{<div>Home</div>}},
            { name:"payment",path:"/payment", component:{<div>Home</div>}},
            { name:"profile",path:"/profile", component:{<div>Home</div>}},
            { name:"setting",path:"/setting", component:{<div>Home</div>}},

        ]
    },
]);
```

